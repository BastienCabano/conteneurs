# ZZ Book

Application microservice pour le cours de Technologie des conteneurs.

CABANO Bastien : bastien.cabano@etu.uca.fr

MARTINEZ Tom : tom.martinez@etu.uca.fr

GARCIA-LAMOUREUX Andy : andy.garcia-lamoureux@etu.uca.fr

## Code des services

Le code des différents services est présent dans `src/`.

* `details` : Service de détails des livres, en ruby.
* `productpage` : Service point d'entrée des autres microservices, en python.
* `reviews` : Service contenant les commentaires sur les livres, appel `ratings`, en java.
* `ratings` : Service de gestion des notes des livres, en node.js.

## Définitions de l'API

L'API visible de l'utilisateur est définie dans le fichier `swagger.yaml`. Pour visualiser le fichier, vous pouvez utiliser le [Swagger editor](https://editor.swagger.io/).



## Cheatsheet

```bash
kubectl delete all --all
docker image prune -a
docker container prune
```


### Lancement :
```bash
docker compose build
./kind load docker-image conteneurs_details conteneurs_mongodb conteneurs_productpage conteneurs_ratings conteneurs_mysql
cd manifests
kubectl apply -f .
cd services
kubectl apply -f .
```


### Debug :
```
kubectl get po
kubectl port-forward <id pod productpage> <9080 int port>:<9080 ext port>
kubectl describe <pod/deployment> <name> 
kubectl get nodes -o wide // pour avoir l'internal ip. 
```
connexion avec `<Internal-IP>:<nodePort 3036>` -> http://172.18.0.2:30036
